#Example App

## Linux 

### Step 1:
```shell
$ python3 -m venv venv
```

### Step 2:

#### activate venv
```shell
$ . venv/bin/activate
```

#### deactivate venv
```shell
$ deactivate
```